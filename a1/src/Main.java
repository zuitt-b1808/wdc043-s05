public class Main {

    public static void main(String[] args) {

        User user1 = new User("Lebron","James",66,"Los Angeles");
        System.out.println("User's first name:");
        user1.callFirstname();
        System.out.println("User's last name:");
        user1.callLastname();
        System.out.println("User's age:");
        user1.callAge();
        System.out.println("User's address:");
        user1.callAddress();
        Course course1 = new Course("Physics 102","Learn Physics",30,1500.00,"June 9,2022","July 4,2022",);
        System.out.println("Course's name:");
        course1.callCourseName();
        System.out.println("Course's description:");
        course1.callCourseDescription();
        System.out.println("Course's seats:");
        course1.callCourseSeats();
        System.out.println("Course's instructor's first name:");
        course1.callCourseInstructor();
    }
}