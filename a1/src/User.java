public class User {

    private String firstName;
    private String lastName;
    private int age;
    private String address;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User(){

    }
    public User(String firstName,String lastName,int age,String address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
    }

    public void callFirstname(){
        System.out.println(this.firstName);
    }
    public void callLastname(){
        System.out.println(this.lastName);
    }

    public void callAge(){
        System.out.println(this.age);
    }

    public void callAddress(){
        System.out.println(this.address);
    }
}
