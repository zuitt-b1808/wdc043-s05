public class Course extends User{

    private String name;
    private String description;
    private int seats;
    private double fee;
    private String startDate;
    private String endDate;
    private User instructor;

    public Course(String name, String learn_physics, int seats, double fee, String startDate, String endDate, User instructor) {
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getSeats() {
        return seats;
    }

    public double getFee() {
        return fee;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public User getInstructor() {
        return instructor;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setInstructor(User instructor) {
        this.instructor = instructor;
    }

    public Course(){

    }

    public Course(String name,String description,int seats,double fee,String startDate,String endDate,User instructor){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = instructor;
    }

    public void callCourseName(){
        System.out.println(this.name);
    }
    public void callCourseDescription(){
        System.out.println(this.description);
    }
    public void callCourseSeats(){
        System.out.println(this.seats);
    }
    public void callCourseInstructor(){
        System.out.println(this.instructor);
    }
}
